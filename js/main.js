class Game {

  _player = null;
  _score = null;
  _lives = null;
  _start = null;
  _container = null;
  _gameOver = null;
  _conDim = null;
  _ball = null;
  _paddle = null;

  constructor() {
    this._init();
  }

  get player() {
    return this._player;
  }

  set player(value) {
    this._player = value;
  }

  get score() {
    return this._score;
  }

  set score(value) {
    this._score = value;
  }

  get lives() {
    return this._lives;
  }

  set lives(value) {
    this._lives = value;
  }

  get start() {
    return this._start;
  }

  set start(value) {
    this._start = value;
  }

  get container() {
    return this._container;
  }

  set container(value) {
    this._container = value;
  }

  get gameOver() {
    return this._gameOver;
  }

  set gameOver(value) {
    this._gameOver = value;
  }

  get conDim() {
    return this._conDim;
  }

  set conDim(value) {
    this._conDim = value;
  }

  get ball() {
    return this._ball;
  }

  set ball(value) {
    this._ball = value;
  }

  get paddle() {
    return this._paddle;
  }

  set paddle(value) {
    this._paddle = value;
  }

  startGame() {
    if (!this.player.gameOver) {
      return;
    }

    this.player.gameOver = false;
    this.gameOver.style.display = "none";
    this.player.score = 0;
    this.player.lives = 3;
    this.ball.style.display = "block";
    //setup bricks
    this._scoreUpdater();
    window.requestAnimationFrame(this._update.bind(this));
  }

  _scoreUpdater() {
    this.score.textContent = this.player.score;
    this.lives.textContent = this.player.lives;
  }

  _init() {
    this.container = document.querySelector('.container');
    this.player = {
      gameOver: true
    };
    this.score = document.getElementById('score');
    this.lives = document.getElementById('lives');
    this.conDim = this.container.getBoundingClientRect();
    this._setGameOver();
    this._setBall();
    this._setPaddle();
    this._setUpArrows();
    window.requestAnimationFrame(() => {
      this._step();
    });
  }

  _setPaddle() {
    this.paddle = document.createElement('div');
    this.paddle.style.position = "absolute";
    this.paddle.style.backgroundColor = "white";
    this.paddle.style.height = "20px";
    this.paddle.style.width = "100px";
    this.paddle.style.borderRadius = "25px";
    this.paddle.style.bottom = "30px";
    this.paddle.style.left = "calc(50% - 50px)";
    this.container.appendChild(this.paddle);
  }

  _setBall() {
    this.ball = document.createElement('div');
    this.ball.style.position = "absolute";
    this.ball.style.width = "20px";
    this.ball.style.height = "20px";
    this.ball.style.backgroundColor = "white";
    this.ball.style.borderRadius = "25px";
    this.ball.style.backgroundImage = "url('img/ball.png')";
    this.ball.style.backgroundSize = "20px 20px";
    this.ball.style.top = "70%";
    this.ball.style.left = "calc(50% - 10px)";
    this.ball.style.display = "none";
    this.container.appendChild(this.ball);
  }

  _setGameOver() {
    this.gameOver = document.createElement('div');
    this.gameOver.textContent = "Start Game";
    this.gameOver.style.position = "absolute";
    this.gameOver.style.color = "white";
    this.gameOver.style.lineHeight = "300px";
    this.gameOver.style.textAlign = "center";
    this.gameOver.style.fontSize = "3em";
    this.gameOver.style.textTransform = "uppercase";
    this.gameOver.style.backgroundColor = "red";
    this.gameOver.style.width = "100%";
    this.gameOver.style.left = "0";
    this.gameOver.style.cursor = "pointer";
    this.gameOver.addEventListener('click', this.startGame.bind(this));
    this.container.appendChild(this.gameOver);
  }

  _setUpArrows() {
    document.addEventListener('keydown', ({key}) => {
      if (key === 'Left' || key === 'ArrowLeft') {
        this.paddle.left = true;
      }
      if (key === 'Right' || key === 'ArrowRight') {
        this.paddle.right = true;
      }
    });

    document.addEventListener('keyup', ({key}) => {
      if (key === 'Left' || key === 'ArrowLeft') {
        this.paddle.left = false;
      }
      if (key === 'Right' || key === 'ArrowRight') {
        this.paddle.right = false;
      }
    });
  }

  _update() {
    let pCurrent = this.paddle.offsetLeft;
    if (this.paddle.left) {
      pCurrent -= 5;
    }
    if (this.paddle.right) {
      pCurrent += 5;
    }
    this.paddle.style.left = `${pCurrent}px`;
    window.requestAnimationFrame(this._update.bind(this));
  }

  _step(timestamp) {
    if (!this.start) {
      this.start = timestamp;
    }
    const progress = timestamp - this.start;
    this.container.style.transform = `translateX(${Math.min(progress / 10, 200)}px)`;
    if (progress < 2000) {
      window.requestAnimationFrame(this._step.bind(this));
    }
  }

}

new Game();
